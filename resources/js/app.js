require('./bootstrap');
import Vue from 'vue'
window.axios = require('axios')
import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)
Vue.component('SystemAlert', require('./components/Alert.vue').default)
import store from './store'
import router from './routes'
import App from './views/App'
const app = new Vue({
    data: {
 
    },
    components: {
        App
    },
    router,
    store
}).$mount('#vueApp')