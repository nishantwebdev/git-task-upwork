import Vue from 'vue'
import Vuex from 'vuex'


import Repository from './modules/repository'
import Alert from './modules/alert'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
    modules: {
        Alert,
        Repository,
    },
    strict: debug,
})
