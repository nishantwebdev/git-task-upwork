function initialState() {
    return {
        repositories: [],
        commits: [],
        relationships: {
            
        },
        query: {},
        loading: false,
    }
}

const getters = {
    repositories:         state => state.repositories,
    commits:         state => state.commits,
    loading:       state => state.loading,
    relationships: state => state.relationships
}

const actions = {
    fetchList({ commit, state },payload) {
        commit('setLoading', true)
        commit('setRepositories', [])

        axios.get('/api/v1/repositories/', { params: payload })
            .then(response => {
                commit('setRepositories', response.data.data)
            })
            .catch(error => {
                var message = error.response.data.message || error.message
              
              this.dispatch('Alert/setAlert', {
                message: message,
                color: 'warning',
              }, 
              {
                root:true
              })
            })
            .finally(() => {
                commit('setLoading', false)
            })
    },
    fetchData({ commit, state },payload) {
        commit('setLoading', true)
        commit('setCommits', [])
        axios.get('/api/v1/repositories/'+payload.id, { params: payload })
            .then(response => {
                commit('setCommits', response.data.data)
            })
            .catch(error => {
                var message = error.response.data.message || error.message
              
              this.dispatch('Alert/setAlert', {
                message: message,
                color: 'warning',
              }, 
              {
                root:true
              })
            })
            .finally(() => {
                commit('setLoading', false)
            })
    },
    setQuery({ commit }, value) {
        commit('setQuery', purify(value))
    },    
}

const mutations = {
    setRepositories(state, repositories) {
        state.repositories = repositories
    },
    setCommits(state, commits) {
        state.commits = commits
    },
    setLoading(state, loading) {
        state.loading = loading
    },
    setQuery(state, query) {
        state.query = query
    },
    resetState(state) {
        state = Object.assign(state, initialState())
    }
}

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
}
