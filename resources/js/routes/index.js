import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter);

import Home from '../views/Home'
import Repositories from '../views/Repositories'
import Repository from '../views/Repository'

import titleMixin from '../mixins/titleMixin'
Vue.mixin(titleMixin)


const routes= [
    {
        path: '/',
        name: 'home',
        component: Home,
        meta: {
            title: 'Home'
        }
    },
    {
        path: '/repositories',
        name: 'repositories',
        component: Repositories,
        meta: {
            title: 'Repositories'
        }
    },
    {
        path: '/repository/:id',
        name: 'repository',
        component: Repository,
        meta: {
            title: 'Repository'
        }
    },
];


const router = new VueRouter({
  mode: "history",
  routes
});

export default router;