<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="api-base-url" content="{{ url('/') }}" />
    <title>Home | Git Task</title>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;900&display=swap" rel="stylesheet">
    <style>
        html, body {
            margin: 0;
            padding: 0;
            font-family: 'Montserrat', sans-serif;
        }
        #mute {
            position: absolute;
        }
        #mute.on {
            opacity: 0.7;
            z-index: 1000;
            background: white;
            height: 100%;
            width: 100%;
            position: fixed;
        }
        .btn:hover {
            box-shadow: 0 0.5rem 1rem rgba(0, 0, 0, 0.15) !important;
        }
        .navbar-nav .nav-link {
	padding: 0;
}
.logo_link{
    text-decoration:none !important;
}

        @media only screen and (min-width: 991px)  {
        #nav-collapse{
          max-width: 100px;
        }
        }


    </style>
</head>
<body>
<div id="mute"></div>
<div id="vueApp">
  <app></app>
</div>
<script src="{{ url('js/app.js') }}"></script>

</body>
</html>