<?php

namespace App\Http\Controllers;

use App\Commit;
use Illuminate\Http\Request;

class CommitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Commit  $commit
     * @return \Illuminate\Http\Response
     */
    public function show(Commit $commit)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Commit  $commit
     * @return \Illuminate\Http\Response
     */
    public function edit(Commit $commit)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Commit  $commit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Commit $commit)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Commit  $commit
     * @return \Illuminate\Http\Response
     */
    public function destroy(Commit $commit)
    {
        //
    }
}
