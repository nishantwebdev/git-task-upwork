<?php

namespace App\Http\Controllers;

use App\Repository;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Http;

class RepositoryController extends Controller
{
    public function sendError($error, $errorMessages = [], $code = 404)
    {
        $response = [
            'success' => false,
            'message' => $error,
        ];

        if(!empty($errorMessages)){
            $response['data'] = $errorMessages;
        }

        return response()->json($response, $code);
    }
    public function sendResponse($result = [], $message)
    {
        $response = [
            'success' => true,
            'message' => $message,
        ];

        if(!empty($result)){
            $response['data'] = $result;
        }

        return response()->json($response, 200);
    }
       
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $repositories = Repository::paginate(25);
        if(count($repositories)){
            $resArr = array_merge($repositories->toArray(),[]);
            return $this->sendResponse($resArr, 'Data retrieve.');
        }else {
            return $this->sendError('No Data Found.', '');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestArr = $request->json()->all();

        $validator = Validator::make($requestArr, [
          'url' => 'required|unique:repositories',
        ], ['url.unique' => 'Repository already exist.',]);
        if($validator->fails()){
            return $this->sendError('Validation Error', $validator->errors());
        }
        $path = parse_url($requestArr['url'], PHP_URL_PATH);
        $pathComponents = explode("/", trim($path, "/"));
        if(count($pathComponents) == 2){
            $response = Http::get('https://api.github.com/repos/'.$pathComponents[0].'/'.$pathComponents[1].'/commits?per_page=30');
            if($response->successful()){
                $commits = $response->json();
                $repository = Repository::create($requestArr);
                if($repository){
                    foreach ($commits as $commit) {
                        $status = $repository->commits()->create([
                            'sha' => $commit['sha'],
                            'message' => $commit['commit']['message'],
                            'author_name' => $commit['commit']['author']['name'],
                            'author_email' => $commit['commit']['author']['email'],
                            'date' => date_format(date_create($commit['commit']['author']['date']), 'Y-m-d H:i:s'),
                          ]);
                    }
                    
                }
            }else {
                $error = $response->json();
                return $this->sendError($error['message'], '');

            }
        }else {
            return $this->sendError('Invalid Repository.', '');
        }        

        $resArr = $repository;
        return $this->sendResponse($resArr, 'Repositry Saved.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Repository  $repository
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        $repository = Repository::find($id);
        if(!empty($repository)){
            $dataArr = $repository->commits()->paginate(25);
            $resArr = array_merge($dataArr->toArray(),['url' => $repository->url]);
            return $this->sendResponse($resArr, 'Data retrieve.');
        }else {
            return $this->sendError('No Data Found.', '');
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Repository  $repository
     * @return \Illuminate\Http\Response
     */
    public function edit(Repository $repository)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Repository  $repository
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Repository $repository)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Repository  $repository
     * @return \Illuminate\Http\Response
     */
    public function destroy(Repository $repository)
    {
        //
    }
}
