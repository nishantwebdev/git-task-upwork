<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Commit extends Model
{
    protected $fillable = ['repository_id', 'sha','message','author_name','author_email','date'];
    protected $hidden = [];

    public function repository()
    {
        return $this->belongsTo(Repository::class, 'repository_id');
    }
}
