<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Repository extends Model
{
    protected $fillable = ['url'];
    protected $hidden = [];

    public function commits()
    {
        return $this->hasMany(Commit::class);
    }
}
